# Knowledge and Data - Final project - Group 35

Code base is build on top of `Vue skeleton`.


## Readme Knowledge and Data

This project is written in VueJs. VueJs is a powerful component-based front-end framework like AnglularJs or ReactJs. To run this project a few dependencies are needed to run the project:

* First make sure you have nodeJS - https://nodejs.org/en/
* The install yarn - https://yarnpkg.com/lang/en/docs/install/#mac-stable
* Clone the repo into your computer (make sure you have git installed. It is possible without git, but we highly recommend to use git)
* In your terminal navigate to your project "cd <PATH OF THE LOCATION OF THE PROJECT>"
* Run in the terminal "yarn"
* All NPM packages are being installed 
* If everything is installed successfully, run "yarn dev". A webpack dev server is starting and project is running now
* For the ontology make sure it's running at url http://localhost:7200/final-assignment (The API calls are expect this url)


### Project structure

This project is setup in the following folders (src as root):

* App - The root component of the project
* asset - fonts, svg, ect....
* component - The folder where all the components are located. Each component has a view (.vue), controller (.js) and style (.scss)
* config - Config of the application
* control - Here starts the application and initial things are loaded
* data - Helper files for data (json with locales, colors files, other data files, ect.)
* directive - Folder with directives
* filter - Global filters
* page - All the pages, same as structure of a component. Within a page (component) are living components
* polyfill - All the polyfills needed (To fix for example IE)
* router - The router files of the application
* settings - Global setting of the application
* store - Data that is saved globally within the application
* utils - Helper files of the application
* vendor - Vendor packages 

## Extra `Vue Skeleton` reading material

### Features `Vue Skeleton`

* [vuex](https://github.com/vuejs/vuex)
* [vue-router](https://github.com/vuejs/vue-router)
* [vue-types](https://github.com/dwightjack/vue-types)
* [webpack](https://github.com/webpack/webpack)
* [SCSS](https://github.com/sass/sass)
* [CSS Modules](https://github.com/css-modules/css-modules)
* [TypeScript](https://github.com/Microsoft/TypeScript)
* [seng-generator](https://github.com/mediamonks/seng-generator)
* [seng-config](https://github.com/mediamonks/seng-config)
* [seng-scss](https://github.com/mediamonks/seng-scss)
* [airbnb coding standard](https://github.com/airbnb/javascript) (integrated with es/ts-lint)
* [modernizr](https://github.com/Modernizr/Modernizr)
* [prettier](https://prettier.io/)
* [lint-staged](https://github.com/okonet/lint-staged)
* [stylelint](https://github.com/stylelint/stylelint) + [stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard)
* i18n using [i18nManager](https://github.com/MatteoGabriele/vue-i18n-manager)
* versioning
* (build) preview server (gzip enabled)
* SVG support
* https support
* [Image optimization](https://github.com/Klathmon/imagemin-webpack-plugin)

### Documentation `Vue Skeleton`
* [Vue Skeleton Documentation](https://github.com/hjeti/vue-skeleton/wiki/Documentation)
* [Webstorm configuration](https://github.com/hjeti/vue-skeleton/wiki/Webstorm-configuration)
* [Skeleton essentials](https://github.com/hjeti/vue-skeleton/wiki/Vue-skeleton-essentials)
