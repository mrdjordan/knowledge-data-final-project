import HomePage from '../page/HomePage';
import Result from '../page/Result';
import Detail from '../page/Detail';

export const RouteNames = {
  HOME: 'home',
  RESULT: 'result',
  DETAIL: 'detail',
};

export default [
  {
    path: '/',
    component: HomePage,
    name: RouteNames.HOME,
  },
  {
    path: '/result',
    component: Result,
    name: RouteNames.RESULT,
  },
  {
    path: '/detail',
    component: Detail,
    name: RouteNames.DETAIL,
  },
];
