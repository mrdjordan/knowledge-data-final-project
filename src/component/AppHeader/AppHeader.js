// @vue/component
import { mapState } from 'vuex';

export default {
  name: 'AppHeader',
  computed: {
    ...mapState({
      searchQuery: state => state.app.searchQuery,
    }),
  },
  beforeMount() {
    if (!this.searchQuery) {
      this.$router.push({ path: '/' });
    }
  },
};
