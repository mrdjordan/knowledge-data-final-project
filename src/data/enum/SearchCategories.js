const SearchCategories = {
  PRODUCT: 'product',
  COMPONENT: 'component',
  COMPANY: 'company',
  CATEGORY: 'category',
};

export default SearchCategories;
