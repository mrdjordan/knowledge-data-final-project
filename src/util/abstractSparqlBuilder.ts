import { GATEWAY, GATEWAY2 } from './../data/Injectables';
import { getValue } from './injector';

export default class AbstractSparqlBuilder {
  constructor(public gateway, public gateway2, public query) {
    this.gateway = getValue(GATEWAY);
    this.gateway2 = getValue(GATEWAY2);
  }

  public initSparqlQuery(): any {
    this.query = `
        PREFIX ex: <http://example.com/resource/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX coco: <http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#>
        PREFIX coco1: <http://example.org/co&co/>
        
        select ?s where { 
            {?s ?p coco:Computer.}
            UNION
            {?s ?p coco:Component.}
            UNION
            {?s rdf:type coco:Company.}
        }
        `;

    return this.gateway.get('final-assignment', {
      params: {
        query: this.query,
        format: 'application/sparql-results+json',
      },
    });
  }

  public checkObject(subject): any {
    this.query = `
        PREFIX ex: <http://example.com/resource/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX coco: <http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#>
        PREFIX coco1: <http://example.org/co&co/>
        
        select ?o ?p where { 
            {${subject} ?p ?o.}
        }
        `;

    return this.gateway.get('final-assignment', {
      params: {
        query: this.query,
        format: 'application/sparql-results+json',
      },
    });
  }

  public getObjects(subject): any {
    this.query = `
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      select * where { 
      ?s rdf:type ${subject} .
      }
        `;

    return this.gateway.get('final-assignment', {
      params: {
        query: this.query,
        format: 'application/sparql-results+json',
      },
    });
  }

  public getComputersWithComponent(subject): any {
    this.query = `
      PREFIX ex: <http://example.com/resource/>
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX coco: <http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#>
      PREFIX coco1: <http://example.org/co&co/>
      
      select ?s where { 
          {${subject} <http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#componentOf> ?s.}
      }

        `;

    return this.gateway.get('final-assignment', {
      params: {
        query: this.query,
        format: 'application/sparql-results+json',
      },
    });
  }

  public getDBPediaData(company): any {
    const upperCase = company.charAt(0).toUpperCase() + company.slice(1);

    this.query = `
      PREFIX dbc: <http://dbpedia.org/resource/Category:>
      PREFIX dct: <http://purl.org/dc/terms/>
      PREFIX db: <http://dbpedia.org/>
      PREFIX dbp: <http://dbpedia.org/property/>
      PREFIX dbo: <http://dbpedia.org/ontology/>
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      
      SELECT ?s ?k ?a  WHERE {
        ?s dct:subject dbc:Computer_hardware_companies .
          ?s rdfs:label ?k .
      #  ?s dbp:name ?k .
        FILTER CONTAINS (str(?k), "${upperCase}" )
        ?s dbo:abstract ?a.
        FILTER (LANG(?a)='en')
      } 
      LIMIT 1
        `;

    return this.gateway2.get('', {
      params: {
        'default-graph-uri': 'http://dbpedia.org',
        query: this.query,
        format: 'application/sparql-results+json',
        CXML_redir_for_subjs: 121,
      },
    });
  }

  public getDBPediaLocation(company): any {
    const upperCase = company.charAt(0).toUpperCase() + company.slice(1);

    this.query = `
      PREFIX geo: <http://www.opengis.net/ont/geosparql#>
      PREFIX dbc: <http://dbpedia.org/resource/Category:>
      PREFIX dct: <http://purl.org/dc/terms/>
      PREFIX db: <http://dbpedia.org/>
      PREFIX dbp: <http://dbpedia.org/property/>
      PREFIX dbo: <http://dbpedia.org/ontology/>
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      
      SELECT DISTINCT ?k ?latd ?longd WHERE {
        ?s dct:subject dbc:Computer_hardware_companies ;
           rdfs:label ?k ;
           dbo:locationCity ?c .
           ?c dbp:latd ?latd ;
           dbp:longd ?longd . 
        FILTER CONTAINS (str(?k), "${upperCase}" )
        ?s dbo:abstract ?a.
        FILTER (LANG(?a)='en')
      } 
      LIMIT 1
        `;

    return this.gateway2.get('', {
      params: {
        'default-graph-uri': 'http://dbpedia.org',
        query: this.query,
        format: 'application/sparql-results+json',
        CXML_redir_for_subjs: 121,
      },
    });
  }
}
