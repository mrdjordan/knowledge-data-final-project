const namespace = 'app';

export const SET_DEVICE_STATE = `${namespace}/setDeviceState`;
export const SET_SEARCH_QUERY = `${namespace}/setSearchQuery`;
export const SET_SEARCH_CATEGORY = `${namespace}/setSearchCategory`;

export default {
  state: {
    deviceState: null,
    searchQuery: null,
    searchCategory: null,
  },
  getters: {},
  mutations: {
    [SET_DEVICE_STATE](state, deviceState) {
      state.deviceState = deviceState;
    },
    [SET_SEARCH_QUERY](state, searchQuery) {
      state.searchQuery = searchQuery;
    },
    [SET_SEARCH_CATEGORY](state, searchCategory) {
      state.searchCategory = searchCategory;
    },
  },
  actions: {},
};
