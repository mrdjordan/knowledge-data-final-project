// @vue/component
import { mapMutations } from 'vuex';
import {
  SET_DEVICE_STATE,
  SET_SEARCH_CATEGORY,
  SET_SEARCH_QUERY,
} from '../../store/module/app/app';
import AbstractSparqlBuilder from '../../util/abstractSparqlBuilder';
import SearchCategories from '../../data/enum/SearchCategories';

export default {
  name: 'SearchBar',
  data() {
    return {
      searchOpen: false,
      searchKeyword: '',
      selectedSearchObject: {},
      searchObjects: [],
      searchResults: [],
      sparqlBuilder: new AbstractSparqlBuilder(),
    };
  },
  mounted() {
    this.sparqlBuilder.initSparqlQuery().then(result => {
      // console.log(result.data.results.bindings);
      result.data.results.bindings.forEach(value => {
        let url = value.s.value;
        url = url
          .split('#')
          .pop()
          .split('?')
          .pop();
        const sub = url
          .substring(url.lastIndexOf('/') + 1)
          .toLowerCase()
          .replace(/_/g, ' ');
        this.searchObjects.push({
          name: sub,
          key: value.s.value,
        });
      });
    });
  },
  methods: {
    ...mapMutations({
      setSearchQuery: SET_SEARCH_QUERY,
      setDeviceState: SET_DEVICE_STATE,
      setSearchCategory: SET_SEARCH_CATEGORY,
    }),
    startSearch() {
      this.searchObjects.forEach(value => {
        // const lowerValue = ;
        if (value.name.indexOf(this.searchKeyword) > -1) {
          this.searchResults.push({
            name: value.name,
            key: value.key,
          });
        }
      });

      this.searchOpen = this.searchKeyword.length > 1;
    },
    removeSearchItems() {
      this.searchResults = [];
    },
    selectResult(selectedItem) {
      this.selectedSearchObject = selectedItem;
      this.searchKeyword = selectedItem.name;
      this.searchOpen = false;
    },
    handleSearch() {
      if (this.selectedSearchObject.key) {
        this.sparqlBuilder.checkObject(`<${this.selectedSearchObject.key}>`).then(result => {
          result.data.results.bindings.forEach(value => {
            const oValue = value.o.value;

            if (oValue.indexOf('Component') > -1) {
              this.setSearchCategory(SearchCategories.COMPONENT);
              this.$router.push({ path: '/result' });
            } else if (
              oValue.indexOf('Portable_computer') > -1 ||
              oValue.indexOf('Desktop_computer') > -1 ||
              oValue.indexOf('Mainframe_computer') > -1 ||
              oValue.indexOf('Supercomputer') > -1
            ) {
              this.setSearchCategory(SearchCategories.PRODUCT);
              this.$router.push({ path: '/result' });
            } else if (oValue.indexOf('Company') > -1) {
              this.setSearchCategory(SearchCategories.COMPANY);
              this.$router.push({ path: '/detail' });
            }
          });
        });
        this.setSearchQuery(this.selectedSearchObject);
      } else {
        this.searchOpen = true;
        this.searchResults = [
          {
            name: 'Desktop Computer',
            key: 'http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#Desktop_computer',
          },
          {
            name: 'Mainframe Computer',
            key:
              'http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#Mainframe_computer',
          },
          {
            name: 'Portable Computer',
            key: 'http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#Portable_computer',
          },
          {
            name: 'Super Computer',
            key: 'http://www.semanticweb.org/home/ontologies/2019/9/FinalProject#Supercomputer',
          },
        ];
      }
    },
  },
};
