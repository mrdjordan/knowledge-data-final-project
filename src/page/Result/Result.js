// @vue/component
import SearchResults from '../../component/SearchResults/SearchResults';
import AppHeader from '../../component/AppHeader/AppHeader';

export default {
  name: 'Result',
  components: {
    AppHeader,
    SearchResults,
  },
};
