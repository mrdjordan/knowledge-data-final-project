// @vue/component

import { mapState } from 'vuex';
import AbstractSparqlBuilder from '../../util/abstractSparqlBuilder';

const GoogleMapsLoader = require('google-maps'); // only for common js environments

export default {
  name: 'DetailComponent',
  data() {
    return {
      sparqlBuilder: new AbstractSparqlBuilder(),
      objectData: {},
      viewReady: false,
      mapVisible: false,
      marker: '',
    };
  },
  computed: {
    ...mapState({
      searchQuery: state => state.app.searchQuery,
      searchCategory: state => state.app.searchCategory,
    }),
  },
  mounted() {
    // this.initMap();
    this.initQuery();
  },
  methods: {
    initMap(lat, long) {
      const myLatLng = {
        lat: Number(lat),
        lng: Number(long),
      };

      GoogleMapsLoader.KEY = 'AIzaSyBKhg3JisePhxbZBTehUaMdwVbD9g6H6mo';
      GoogleMapsLoader.VERSION = '3.35';
      GoogleMapsLoader.LIBRARIES = ['drawing', 'geometry', 'places'];

      GoogleMapsLoader.load(google => {
        const map = new google.maps.Map(this.$refs.mapWrapper, {
          center: myLatLng,
          zoom: 1,
          disableDefaultUI: true,
          zoomControl: false,
          scaleControl: false,
        });
        // eslint-disable-next-line
        this.marker = new google.maps.Marker({ position: myLatLng, map: map });
        // eslint-disable-next-line
        // const marker2 = new google.maps.Marker({ position: myLatLng2, map: map });
      });
    },
    initQuery() {
      this.sparqlBuilder.getDBPediaData(this.searchQuery.name).then(result => {
        result.data.results.bindings.forEach(value => {
          this.objectData.name = value.k;
          this.objectData.description = value.a;
        });

        this.viewReady = true;
      });

      this.sparqlBuilder.getDBPediaLocation(this.searchQuery.name).then(result => {
        result.data.results.bindings
          .forEach(value => {
            this.mapVisible = true;
            this.initMap(value.latd.value, value.longd.value);
          })
          .reject(() => {
            this.mapVisible = false;
          });
      });
    },
  },
  beforeDestroy() {
    this.marker = null;
    GoogleMapsLoader.release(function() {});
    this.mapVisible = false;
  },
};
