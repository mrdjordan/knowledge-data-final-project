// @vue/component
import { mapMutations, mapState } from 'vuex';
import AbstractSparqlBuilder from '../../util/abstractSparqlBuilder';
import SearchCategories from '../../data/enum/SearchCategories';
import {
  SET_DEVICE_STATE,
  SET_SEARCH_CATEGORY,
  SET_SEARCH_QUERY,
} from '../../store/module/app/app';

export default {
  name: 'SearchResults',
  data() {
    return {
      detailOpen: false,
      sparqlBuilder: new AbstractSparqlBuilder(),
      searchObjects: [],
      callReady: false,
    };
  },
  computed: {
    ...mapState({
      searchQuery: state => state.app.searchQuery,
      searchCategory: state => state.app.searchCategory,
    }),
  },
  created() {
    this.initSearchResults();
  },
  methods: {
    ...mapMutations({
      setSearchQuery: SET_SEARCH_QUERY,
      setDeviceState: SET_DEVICE_STATE,
      setSearchCategory: SET_SEARCH_CATEGORY,
    }),
    initSearchResults() {
      if (this.searchCategory === SearchCategories.PRODUCT) {
        if (this.searchQuery.key.indexOf('Desktop_computer') > -1) {
          this.sparqlBuilder.getObjects(`<${this.searchQuery.key}>`).then(result => {
            this.bindValues(result.data.results.bindings);
          });
        } else if (this.searchQuery.key.indexOf('Mainframe_computer') > -1) {
          this.sparqlBuilder.getObjects(`<${this.searchQuery.key}>`).then(result => {
            this.bindValues(result.data.results.bindings);
          });
        } else if (
          this.searchQuery.key.indexOf('Portable_computer') > -1 ||
          this.searchQuery.key.indexOf('Laptop') > -1 ||
          this.searchQuery.key.indexOf('laptop_name') > -1
        ) {
          this.sparqlBuilder.getObjects(`<${this.searchQuery.key}>`).then(result => {
            this.bindValues(result.data.results.bindings);
          });
        } else if (this.searchQuery.key.indexOf('Supercomputer') > -1) {
          this.sparqlBuilder.getObjects(`<${this.searchQuery.key}>`).then(result => {
            this.bindValues(result.data.results.bindings);
          });
        } else {
          this.sparqlBuilder.checkObject(`<${this.searchQuery.key}>`).then(result => {
            this.searchObjects = [{ name: this.searchQuery.name }];
            this.bindSeparateValues(result.data.results.bindings);
          });
        }
      } else if (this.searchCategory === SearchCategories.COMPONENT) {
        this.sparqlBuilder.getComputersWithComponent(`<${this.searchQuery.key}>`).then(result => {
          this.bindValues(result.data.results.bindings);
        });
      }
    },
    bindSeparateValues(result) {
      const attr = {};
      attr.hasGPU = {};
      attr.hasDiskSpace = {};
      attr.hasDisplaySize = {};
      attr.hasCPU = {};
      attr.company = {};

      result.forEach(value => {
        const pValue = value.p.value;
        const oValue = value.o.value;

        if (pValue.indexOf('hasGPU') > -1) {
          attr.hasGPU.key = oValue;
          attr.hasGPU.name = oValue
            .split('#')
            .pop()
            .split('?')
            .pop()
            .substring(oValue.lastIndexOf('/') + 1)
            .toLowerCase()
            .replace(/_/g, ' ');
        } else if (pValue.indexOf('hasDiskSpace') > -1) {
          attr.hasDiskSpace.key = oValue;
          attr.hasDiskSpace.name = oValue
            .split('#')
            .pop()
            .split('?')
            .pop()
            .substring(oValue.lastIndexOf('/') + 1)
            .toLowerCase()
            .replace(/_/g, ' ');
        } else if (pValue.indexOf('hasDisplaySize') > -1) {
          attr.hasDisplaySize.key = oValue;
          attr.hasDisplaySize.name = oValue
            .split('#')
            .pop()
            .split('?')
            .pop()
            .substring(oValue.lastIndexOf('/') + 1)
            .toLowerCase()
            .replace(/_/g, ' ');
        } else if (pValue.indexOf('hasCPU') > -1) {
          attr.hasCPU.key = oValue;
          attr.hasCPU.name = oValue
            .split('#')
            .pop()
            .split('?')
            .pop()
            .substring(oValue.lastIndexOf('/') + 1)
            .toLowerCase()
            .replace(/_/g, ' ');
        } else if (oValue.indexOf('Portable_computer') > -1) {
          attr.type = 'Laptop';
        } else if (oValue.indexOf('Company') > -1) {
          attr.company.key = pValue;
          attr.company.name = pValue
            .split('#')
            .pop()
            .split('?')
            .pop()
            .substring(pValue.lastIndexOf('/') + 1)
            .toLowerCase()
            .replace(/_/g, ' ');
        }
      });

      this.searchObjects[0].attr = attr;
      this.callReady = true;
    },
    bindValues(result) {
      result.forEach(resultItem => {
        const uri = resultItem.s.value;
        this.sparqlBuilder.checkObject(`<${uri}>`).then(el => {
          const attr = {};
          attr.hasGPU = {};
          attr.hasDiskSpace = {};
          attr.hasDisplaySize = {};
          attr.hasCPU = {};
          attr.company = {};

          el.data.results.bindings.forEach(value => {
            const pValue = value.p.value;
            const oValue = value.o.value;

            if (pValue.indexOf('hasGPU') > -1) {
              attr.hasGPU.key = oValue;
              attr.hasGPU.name = oValue
                .split('#')
                .pop()
                .split('?')
                .pop()
                .substring(oValue.lastIndexOf('/') + 1)
                .toLowerCase()
                .replace(/_/g, ' ');
            } else if (pValue.indexOf('hasDiskSpace') > -1) {
              attr.hasDiskSpace.key = oValue;
              attr.hasDiskSpace.name = oValue
                .split('#')
                .pop()
                .split('?')
                .pop()
                .substring(oValue.lastIndexOf('/') + 1)
                .toLowerCase()
                .replace(/_/g, ' ');
            } else if (pValue.indexOf('hasDisplaySize') > -1) {
              attr.hasDisplaySize.key = oValue;
              attr.hasDisplaySize.name = oValue
                .split('#')
                .pop()
                .split('?')
                .pop()
                .substring(oValue.lastIndexOf('/') + 1)
                .toLowerCase()
                .replace(/_/g, ' ');
            } else if (pValue.indexOf('hasCPU') > -1) {
              attr.hasCPU.key = oValue;
              attr.hasCPU.name = oValue
                .split('#')
                .pop()
                .split('?')
                .pop()
                .substring(oValue.lastIndexOf('/') + 1)
                .toLowerCase()
                .replace(/_/g, ' ');
            } else if (oValue.indexOf('Portable_computer') > -1) {
              attr.type = 'Laptop';
            } else if (oValue.indexOf('Company') > -1) {
              attr.company.key = pValue;
              attr.company.value = pValue
                .split('#')
                .pop()
                .split('?')
                .pop()
                .substring(pValue.lastIndexOf('/') + 1)
                .toLowerCase()
                .replace(/_/g, ' ');
            }
          });

          this.searchObjects.push({
            name: uri
              .split('#')
              .pop()
              .split('?')
              .pop()
              .substring(uri.lastIndexOf('/') + 1)
              .toLowerCase()
              .replace(/_/g, ' '),
            attr,
          });
        });
      });

      this.callReady = true;
    },
    toggleDetail(e) {
      const item = e.target.closest('.js-result-item');
      if (!item.classList.contains('is-open')) {
        this.$el.querySelectorAll('.js-result-item').forEach(el => {
          el.closest('.js-result-item').classList.remove('is-open');
          el.closest('.js-result-item').querySelector('.js-detail-wrapper').style.maxHeight = 0;
        });

        // eslint-disable-next-line
        const elHeight = item.querySelector('.js-detail-wrapper').scrollHeight;

        item.querySelector('.js-detail-wrapper').style.maxHeight = String(elHeight).concat(
          '',
          'px',
        );
        item.classList.add('is-open');
      } else {
        item.querySelector('.js-detail-wrapper').style.maxHeight = 0;
        item.classList.remove('is-open');
      }
    },
    handleDeeplink(key, name) {
      this.callReady = false;
      this.searchObjects = [];
      this.setSearchCategory(SearchCategories.COMPONENT);

      this.setSearchQuery({
        name,
        key,
      });

      this.initSearchResults();
    },
  },
};
