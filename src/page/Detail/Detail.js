// @vue/component
import AppHeader from '../../component/AppHeader/AppHeader';
import DetailComponent from '../../component/DetailComponent/DetailComponent';

export default {
  name: 'Detail',
  components: {
    AppHeader,
    DetailComponent,
  },
};
