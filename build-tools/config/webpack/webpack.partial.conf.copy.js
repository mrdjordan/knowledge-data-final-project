const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = ({ config, isDevelopment }) => webpackConfig => {
  return {
    ...webpackConfig,
    context: path.join(config.projectRoot),
    plugins: [
      new CopyWebpackPlugin([
        { from: '/health.html', to: '/dist' },
        { logLevel: 'debug' }
      ])
    ]
  }
};