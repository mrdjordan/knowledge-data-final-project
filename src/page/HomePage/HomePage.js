// @vue/component
import SearchBar from '../../component/SearchBar/SearchBar';

export default {
  name: 'HomePage',
  components: {
    SearchBar,
  },
};
